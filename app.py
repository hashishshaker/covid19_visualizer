import scripts
import datetime

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

states_list = scripts.get_state_codes()
drop_down_list = []

for (state_code, state_name) in states_list.items():
    drop_down_list.append({'label': state_name, 'value': state_code})

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    dcc.Dropdown(
        id='state_choice',
        options=drop_down_list,
        value='mh'
    ),
    dcc.Dropdown(
        id='view_cases_by',
        options=[{'label': 'daily', 'value': 1},
                 {'label': 'weekly', 'value': 7}],
        value=1
    ),
    dcc.Graph(
        id='graph'
    )
])


@app.callback(
    Output('graph', 'figure'),
    [Input('state_choice', 'value'),
     Input('view_cases_by', 'value')])
def update_graph(selected_state, view_cases_by):
    state_data = scripts.get_daily_new_cases_and_cumulative_cases(selected_state)

    if view_cases_by == 1:
        return {
            'data': [
                {'x': state_data['dates'],
                 'y': state_data['new_daily_cases'],
                 'type': 'bar'
                 }
            ],
            'layout': {
                'title': 'Number of confirmed cases in ' + state_data['state']
            }
        }
    elif view_cases_by == 7:
        date_today = datetime.date.today()
        x = []
        y = []

        bucket_start_date = datetime.datetime.strptime(state_data['dates'][0], '%d-%b-%y').date()
        bucket_end_date = bucket_start_date + datetime.timedelta(days=7)

        while bucket_end_date <= date_today:
            x.append('(' + bucket_start_date.strftime('%d-%b-%y') + " - " + bucket_end_date.strftime('%d-%b-%y') + ']')
            num_cases = 0
            for dt in state_data['dates']:
                if bucket_start_date < datetime.datetime.strptime(dt, '%d-%b-%y').date() <= bucket_end_date:
                    num_cases = num_cases + int(state_data['new_daily_cases'][state_data['dates'].index(dt)])

            y.append(num_cases)
            bucket_start_date = bucket_end_date
            bucket_end_date = bucket_start_date + datetime.timedelta(days=7)

        x.append('(' + bucket_start_date.strftime('%d-%b-%y') + " - " + date_today.strftime('%d-%b-%y') + ']')
        num_cases = 0
        for dt in state_data['dates']:
            if bucket_start_date < datetime.datetime.strptime(dt, '%d-%b-%y').date() <= date_today:
                num_cases = num_cases + int(state_data['new_daily_cases'][state_data['dates'].index(dt)])

        y.append(num_cases)

        return {
            'data': [
                {'x': x,
                 'y': y,
                 'type': 'bar',
                 'text': y,
                 'textposition': 'auto'
                 }
            ],
            'layout': {
                'title': 'Number of confirmed cases in ' + state_data['state']
            }
        }


if __name__ == '__main__':
    app.run_server(debug=True)
