import json
import urllib.request

import plotly.graph_objects as go
from pandas import DataFrame

source_data = "https://api.covid19india.org/states_daily.json"


def get_state_codes():
    states_codes = 'states_codes.json'
    with open(states_codes) as f:
        states_codes_json = json.load(f)

    return states_codes_json


def get_daily_new_cases_and_cumulative_cases(state_code):
    response = urllib.request.urlopen(source_data)
    data = json.loads(response.read())

    df = DataFrame(data["states_daily"])

    tg_data = df[['date', 'status', state_code]]

    x = []
    y = []
    y_cumulative = []

    confirmed_cases_state = tg_data.loc[df['status'] == 'Confirmed']

    for q in confirmed_cases_state['date']:
        x.append(q)

    for w in confirmed_cases_state[state_code]:
        if w == "":
            w = 0

        y.append(w)

        if len(y) == 1:
            y_cumulative.append(w)
        else:
            y_cumulative.append(int(y_cumulative[len(y_cumulative) - 1]) + int(w))

    return {
        'state': get_state_name_from_code(state_code),
        'dates': x,
        'new_daily_cases': y,
        'cumulative_cases': y_cumulative
    }


def get_individual_state_new_cumulative_cases(state_code):
    state_name = get_state_name_from_code(state_code)
    result = get_daily_new_cases_and_cumulative_cases(state_code)

    fig = go.Figure()

    fig.add_trace(go.Bar(
        x=result['dates'], y=result['new_daily_cases'],
        text=result['new_daily_cases'],
        textposition='auto',
        name='Number of daily new cases'
    ))

    fig.add_trace(go.Scatter(
        x=result['dates'], y=result['cumulative_cases'],
        text=result['cumulative_cases'],
        name='Cumulative number of new daily cases'
    ))

    fig.update_layout(
        title={
            'text': "Number of daily new cases and cumulative number of infections in " + state_name,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'
        }
    )

    fig.show()


def get_state_name_from_code(state_code):
    return get_state_codes()[state_code]


def get_states_data(states_code_list):
    fig = go.Figure()
    annotations = []
    states_codes = 'states_codes.json'
    with open(states_codes) as f:
        states_codes_json = json.load(f)

    for state_code in states_code_list:
        state_cases = get_daily_new_cases_and_cumulative_cases(state_code)
        fig.add_trace(
            go.Scatter(x=state_cases['dates'], y=state_cases['cumulative_cases'], name=states_codes_json[state_code]))

    fig.show()
