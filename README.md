# Covid-19 India visualization #

This file contains the steps needed to run the code.

### What is this repository for? ###

* This repository contains code for visualizing the state-wise trends in growth of Covid-19 cases in India
* Version 1.0
* The data is sourced from [this Github repository](https://github.com/covid19india)

### How do I get set up? ###

* Either clone the master branch or download the repository from [here](https://bitbucket.org/hashishshaker/covid19_visualizer/src/master/)
* Dependencies are pandas and dash. Visual C++ Build Tools would be needed to install pandas. The tools can be downloaded [here](https://visualstudio.microsoft.com/visual-cpp-build-tools/).
* Once Visual C++ Build Tools are installed, the relevant installation for pandas can be found [here](https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html)
* The installation details of dash can be found [here](https://dash.plotly.com/installation)
* No database configuration is needed for this repository
* Once the prerequisites are installed, navigate to the location of 'app.py' in command prompt on your system, type 'python app.py' and press enter.
* A local server will be run on your system and the ip will be listed in the command prompt. Navigate to the url in the browser on the same computer you are running your code on to open the visualizer.

### Who do I talk to? ###

* Contact repo owner in case you notice any bugs or have suggestions for any enhancements
